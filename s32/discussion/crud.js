let http = require('http');
let port = 4000;

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Taylor",
		"email" : "tswift@mail.com"
	}
]

let server = http.createServer((request, response) => {
	// checks if the url and the method matches the required condition
	if (request.url == "/users" && request.method == "GET"){
		// content-type is set to applicatio/json because in real-world management of databases, we are working with objects and less texts.
		response.writeHead(200, {"Content-Type" : "application/json"});
		// the frontend and backend applications communicate through strings. we have to convert the JSON data type from the server into stringified JSON when it is sent to the frontend application
		response.write(JSON.stringify(directory));
		// ends the response process
		response.end();
	}

	if (request.url == "/users" && request.method == "POST"){
		let requestBody = '';
		request.on('data', function(data){
			requestBody += data;
		});
		request.on('end', function(){
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			directory.push(newUser);
			console.log(newUser);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end();
		})
		
	}
})
server.listen(port);
console.log(`Server running at localhost:${port}`);
