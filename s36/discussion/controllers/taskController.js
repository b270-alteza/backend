// Controllers contain the functions and business logic of our Express application
// All the operations that it can do will be placed in this file
const Task = require("../models/Task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.createTask = (requestBody) => {

	// Creates a task object based on the model Task
	let newTask = new Task({

		// Sets the name property with the value received from the client/postman
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {

		if(error){
			console.log(error);
			return false
		} else {
			return task;
		}
	})
}


// module.exports.deleteTask = (taskId) => {

// 	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
// 		if(err) {
// 			console.log(err);
// 			return false
// 		} else {
// 			return removedTask;
// 		}
// 	})
// }

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false
		} else {
			return removedTask;
		}
	})
}



module.exports.updateTask = (taskId,newContent) => {

	return Task.findById(taskId).then((result, error) => {

		if(error) {
			console.log(error);
			return false;
		}

		// Result of the "findById" will be stored in the "result" parameter
		// Its "property" will be reassigned to the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

// Activity - Start

module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then(result => {
        return result
    })
};

module.exports.updateTask = (taskId,newContent) => {

    return Task.findById(taskId).then((result, error) => {

        if(error) {
            console.log(error);
            return false;
        }

        result.status = "complete";

        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTask;
            }
        })
    })
}

// Activity - End