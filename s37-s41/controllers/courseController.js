const Course = require("../models/Course");
const auth = require("../auth");
// Create a new course
/*
	Steps:
		1. Create a conditional statement that will check if the user is an administrator.
		2. Create a new Course object using the mongoose model and the information from the request body
		3. Save the new Course to the database
*/

module.exports.addCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		});

		// Saves the created object to our database
		return newCourse.save()
		// Course creation is successful
		.then(course => {
			console.log(course);
			res.send(true);
		})
		// Course creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
	
}

// Retrieve all courses
/*
	Step:
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = (req, res) => {

	return Course.find({}).then(result => res.send(result));
}


// Retrieve all ACTIVE courses
/*
	Step:
	1. Retrieve all the courses from the database with property of "isActive: true"
*/
module.exports.getAllActive = (req, res) => {

	return Course.find({isActive: true}).then(result => res.send(result));
}


// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided in the URL
*/
module.exports.getCourse = (req, res) => {

	console.log(req.params.courseId)

	return Course.findById(req.params.courseId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}


// Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateCourse = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			slots: req.body.slots
		}

		// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
		// {new:true} - returns the updated document
		return Course.findByIdAndUpdate(req.params.courseId, updateCourse, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
		// return res.send("You don't have access to this page!")
	}
}








