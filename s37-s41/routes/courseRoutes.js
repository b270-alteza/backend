const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course
// router.post("/", courseController.addCourse);
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.getAllCourses);

// Route for retrieving all ACTIVE courses
router.get("/", courseController.getAllActive);

// Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", courseController.getCourse);

// Route for updating a course
router.put("/:courseId", auth.verify, courseController.updateCourse);

module.exports = router;