/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function getPersonalDetails() {
	let fullName = prompt("What is your name?");
	let age = prompt("How old are you? \n (Number only)");
	while (isNaN(age)) {
		age = prompt("Please enter only numbers for age. \nHow old are you?");
	}
	let location = prompt("Where do you live?");

	console.log("Hello, " + fullName); 
	console.log("You are " + age + " years old."); 
	console.log("You live in " + location); 
};

getPersonalDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function getBands() {
	const bandInput = prompt("Enter the list of bands or musical artists that you like.  \n Separate by commas: (e.g. Lady Gaga, Ariana Grande)");
	if (!bandInput) {
		return []; // return an empty array if the user cancels or inputs nothing
	}

	const bands = bandInput.split(", ");

	for (let i = 0; i < bands.length; i++) {
		console.log((i+1) + ". " + bands[i]);
	}
};

getBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function showMoviesAndRatings() {
	const movies = ["Click", "Hunger Games", "Avatar", "Passengers", "Predestination", "In Time"] 
	const movieRatings = [34, 87, 82, 30, 84, 37]

	for (let i = 0; i < movies.length; i++) {
		console.log((i+1) + ". " + movies[i]);
		console.log("Rotten Tomatoes Rating: " + movieRatings[i] + "%");
	}
};

showMoviesAndRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers(); 
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
