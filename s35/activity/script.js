

// Asynchronous means that we can proceed to execute other statements while time-consuming code is running in the background

// [SECTION] Getting all posts
// Fetch API allows us to asynchronously request for a resource(data)
// It can be used to fetch various types of resources: text, JSON, HTML, images, and more.
// fethc() method in JS that is used to request to a server and load the information on the webpage
/*
	Syntax: fetch("apiuRL")
*/

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then(response => console.log(response.status))


// // Returns a "promise"
// // A "promise" is an object that represents the eventual completion or failure of an asychronous function and its resulting value
// 	// A "promise" may be in one of 3 possible states:
// 		// 1. pending - initial state, neither fulfilled nor rejected
// 		// 2. fulfilled - operation was completed
// 		// 3. rejected - operation failed
// console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET"
})
.then(response => response.json())
.then(response => {
	let titles = response.map(post => post.title)
	console.dir(titles)
})

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "GET"
})
.then(response => {
	console.log(response.status);
	return response.json();
})
.then(response => console.log(`Title: ${response.title}`));

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Add Post",
		body: "This is a new post",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

// [SECTION] Updating a post
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "This is an updated post",
		body: "Update contents",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title : "Patched title",
		description : "Patched desc",
		status : "Complete",
		dateCompleted : "4/27/2023",
	    userId : 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));

