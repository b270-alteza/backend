const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://admin:admin123@zuitt.yipwdbt.mongodb.net/b270-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log('We are connected to the database'));

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true,
	},
	password: {
		type: String
	}
})

const User = mongoose.model("User", userSchema);

app.use( express.json() );
app.use( express.urlencoded( { extended:true } ) )

app.post('/signup', (req, res) => {
	User.findOne({ username : req.body.username }).then((result, err)=>{
		if (result !== null && result.username === req.body.username){
			return res.send("Duplicate User found");
		}else{
			let newUser = new User({
				username : req.body.username,
				password : req.body.password
			})
			newUser.save().then((savedUser, error) => {
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New User Created")
				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at  ${port}`) );