// console.log("Hello World!");

// [SECTION] JavaScript Synchronous and Asynchronous
// Javascript by default is synchronous meaning only one statement is executed at a time
// This can be proven when a statement has an error, JS will not proceed with the next statement
console.log("Hello World!");
// conosle.log("Hello Again!");
console.log("Goodbye");

// Code blocking - waiting for the specific statement to finish before executing the next statement
// for(let i = 0; i <= 1500; i++){
// 	console.log(i)
// }
console.log("Hello again!");

// Asynchronous means that we can proceed to execute other statements while time-consuming code is running in the background

// [SECTION] Getting all posts
// Fetch API allows us to asynchronously request for a resource(data)
// It can be used to fetch various types of resources: text, JSON, HTML, images, and more.
// fethc() method in JS that is used to request to a server and load the information on the webpage
/*
	Syntax: fetch("apiuRL")
*/

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status))

console.log("Ends here ------------------------------------------")

// Returns a "promise"
// A "promise" is an object that represents the eventual completion or failure of an asychronous function and its resulting value
	// A "promise" may be in one of 3 possible states:
		// 1. pending - initial state, neither fulfilled nor rejected
		// 2. fulfilled - operation was completed
		// 3. rejected - operation failed
console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

fetch("https://jsonplaceholder.typicode.com/posts")
// We used "json()" from the "response" object to convert the data retrieved to JS Object
.then(response => response.json())
// .then(response => console.log(response))
.then(response => {
	response.forEach(post => console.log(post.title))
})


// "async" and "await" keywords are another approach that can be used to achieve asynchronous code

async function fetchData(){

	// waits for the fetch to complete then stores the value in the result variable
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
	console.log(result);

	// Converts "result" into JS object
	let json = await result.json();
	console.log(json);
}
fetchData();


// [SECTION] Getting a specific post
// ":id" is wildcard where we can put any value
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((response) => console.log(response));

// [SECTION] Creating a post
/*
	Syntax:
		fetch("URL", {options})
		.then()
*/
fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
})
.then(response => response.json())
.then(response => console.log(response));

// [SECTION] Updating a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [SECTION] Updating a post using the PATCH method
// PUT vs PATCH
	// PATCH is used to update a single/several properties
	// PUT is ued to update the whole document 

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title",
		
	})
})
.then(response => response.json())
.then(json => console.log(json));


// [SECTION] Delete a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})
.then(response => response.json())
.then(response => console.log(response));

