let myTrainer = {
    name: "Ash Ketchum",
    age: 10,
    friend: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function(){
        console.log("Pikachu! I choose you!");
    }
};

console.log(myTrainer);

console.log("Result of dot notation:");
console.log(myTrainer.name);

console.log("Result of square bracket notation:");
console.log(myTrainer["pokemon"]);

console.log("Result to talk method:");
myTrainer.talk();

function Pokemon(name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
    this.tackle = function(target) {
        console.log(this.name + " tackled " + target.name);

        target.health -= this.attack;

        console.log(this.name + "'s health is now reduced to" + target.health);

        if (target.health <= 0) {
            target.faint();
        };

        console.log(target);
    };
    this.faint = function(){
        console.log(this.name + " fainted.");
    }
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);