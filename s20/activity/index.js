let number = prompt("Input any positive integer");

console.log("The number provided is " + number + ".")

for (number; number > 0; number--) {
	if (number <= 50) {
		console.log("The current value is " + number + ". Terminating the loop.")
		break;
	}
	if (number % 10 == 0) {
		console.log("The number us divisible by 10. Skipping the number.");
		continue;
	}
	if (number % 5 == 0) {
		console.log(number);
	}
}

let word = "supercalifragilisticexpialidocious";
let onlyConsonants = "";

for (let i = 0 ; i < word.length ; i++) {
	if (
	        word[i].toLowerCase() == "a" ||
	        word[i].toLowerCase() == "e" ||
	        word[i].toLowerCase() == "i" ||
	        word[i].toLowerCase() == "o" ||
	        word[i].toLowerCase() == "u"
	    ) {
		continue;
	} else {
		onlyConsonants += word[i];
	}
}

console.log(word);
console.log(onlyConsonants);