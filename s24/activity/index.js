let getCube = 2 ** 3;

console.log(`The cube of 2 is ${getCube} `);

let address = ["Marcopper", "Sta. Cruz", "Marinduque"];

const [baranggay, town, province] = address;

console.log(`I live at ${baranggay}, ${town}, ${province}.`);

let animal = {
    dog: "Husky",
    cat: "Lion",
    bird: "Parrot"
};

let { dog, cat, bird } = animal;

console.log(`I love ${dog}s, I hate ${cat}s and ${bird}s are annoying`);

let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(`${number}`);
});

const sum = numbers.reduce((accumulator, currentValue) => {
  return accumulator + currentValue;
}, 0);

console.log(sum);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const myDog = new Dog();
myDog.name = "aso";
myDog.age = 5;
myDog.breed = "Husky";

console.log(myDog);